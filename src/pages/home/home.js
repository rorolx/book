import React, { Component } from "react";
import Header from "../../components/header/header.js";
import Library from "../../components/library/library.js";

class Home extends Component {
  render() {
    return (
      <div className="App">
        <Header title="Livres" />

        <Library />
      </div>
    );
  }
}

export default Home;
