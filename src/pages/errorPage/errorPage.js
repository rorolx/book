import React, { Component } from "react";
import Header from "../../components/header/header.js";

class ErrorPage extends Component {
  render() {
    console.log("props:: ", this.props);
    return (
      <div>
        <Header title="Error" />
        <div className="error">Erreur 404</div>
        <div>La page "{this.props.location.pathname}" n'existe pas </div>
      </div>
    );
  }
}

export default ErrorPage;
