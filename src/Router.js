import React, { Component } from "react";
import { Switch, Route } from "react-router-dom";

import Home from "./pages/home/home.js";
import CartPage from "./pages/cartPage/cartPage.js";
import Book from "./pages/book/book.js";
import ErrorPage from "./pages/errorPage/errorPage.js";

class Router extends Component {
  render() {
    return (
      <Switch>
        <Route exact path="/" component={Home} />
        <Route exact path="/cart" component={CartPage} />
        <Route exact path="/book/:id" component={Book} />
        <Route component={ErrorPage} />
      </Switch>
    );
  }
}

export default Router;
