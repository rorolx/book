import React, { Component } from "react";

class Books extends Component {
  addToShoppingCart(props) {
    console.log("addToShoppingCart:: ", props);
  }
  render() {
    return (
      <div>
        <img
          className="cover"
          src={this.props.cover}
          alt={this.props.title}
          width="100"
        />
        <p className="title">{this.props.title}</p>
        <p className="price">{this.props.price} €</p>
        <button onClick={e => this.addToShoppingCart(this.props)}>
          Ajouter au panier
        </button>
        <p className="synopsis">{this.props.synopsis}</p>
      </div>
    );
  }
}

export default Books;
