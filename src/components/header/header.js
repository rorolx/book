import React, { Component } from "react";
import Cart from "../../components/cart/cart.js";
import Navbar from "../../components/navbar/navbar.js";

class Header extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <div>
        <header className="App-header">
          <h1>{this.props.title}</h1>
          <Cart />
        </header>
        <Navbar />
      </div>
    );
  }
}

export default Header;
