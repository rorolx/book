import React from "react";
import { NavLink } from "react-router-dom";
import "./navbar.css";

export default function Navbar() {
  return (
    <nav>
      <NavLink exact to={"/"}>
        Accueil
      </NavLink>
      &nbsp;|&nbsp;
      <NavLink exact to={"/cart"}>
        Panier
      </NavLink>
    </nav>
  );
}
