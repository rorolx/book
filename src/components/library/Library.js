import React, { Component } from "react";
import Books from "../books/books";
import "./library.css";

class Library extends Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      searchString: "",
      library: []
    };
  }

  componentDidMount() {
    fetch("books.json")
      .then(response => response.json())
      .then(
        // result
        result => {
          this.setState({
            isLoaded: true,
            library: result
          });
          this.refs.search.focus();
          console.log(result);
        },
        // error
        error => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      );
  }

  handleChange = () => {
    this.setState({
      searchString: this.refs.search.value
    });
  };

  render() {
    let { error, isLoaded, searchString, library } = this.state;
    let search = searchString.trim().toLowerCase();

    //            console.log("STATE:: ", library);
    if (search.length > 0) {
      library = library.filter(book => {
        return (
          book.isbn.match(search) ||
          book.title.toLowerCase().match(search) ||
          book.synopsis
            .join(",")
            .toLowerCase()
            .match(search) ||
          book.price.toString().match(search)
        );
      });
    }

    if (error) {
      return <div>Error in loading</div>;
    } else if (!isLoaded) {
      return <div>Loading ...</div>;
    } else {
      return (
        <div>
          <input
            className="search"
            type="text"
            value={this.state.searchString}
            ref="search"
            onChange={this.handleChange}
            placeholder="rechercher"
          />

          <ul className="book">
            {library.map(book => (
              <li key={book.isbn} id={book.isbn}>
                <Books
                  isbn={book.isbn}
                  cover={book.cover}
                  title={book.title}
                  price={book.price}
                  synopsis={book.synopsis}
                />
              </li>
            ))}
          </ul>
        </div>
      );
    }
  }
}

export default Library;
